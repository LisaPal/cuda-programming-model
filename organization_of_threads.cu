#include "cuda_runtime.h"
#include "device_launch_parameters.h"

#include <stdio.h>

// Video 6 kernel
__global__ void print_threadIds(){

	printf("threadIdx.x : %d, threadIdx.y : %d, threadIdx.z : %d \n",
	 threadIdx.x, threadIdx.y, threadIdx.z);
}


// Video 7 kernel 
__global__ void print_details(){
		printf("blockIdx.x : %d, blockIdx.y : %d, blockIdx.z : %d, blockDim.x: %d, blockDim.y: %d, gridDim.x : %d, gridDim.y: %d \n",
	 blockIdx.x, blockIdx.y, blockIdx.z, blockDim.x, blockDim.y, gridDim.x, gridDim.y);

}

int main(){
    int nx, ny; 
    // nx : # threads in x-dim. ny: # threads in y-dim.
    nx = 16;
    ny = 16; 

    // Block has 8 threads in x-dim and 8 threads in y-dim.
    dim3 block(8,8); 
    // Get # blocks in grid
    dim3 grid(nx/ block.x, ny/block.y);

    // Launch kernel with launch parameters
    print_details << <grid,block >> > (); 
    // Wait until kernel execution is finished
    cudaDeviceSynchronize(); 

	cudaDeviceReset(); 
	return 0; 
}

