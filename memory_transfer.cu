#include "cuda_runtime.h" 
#include "device_launch_parameters.h"

#include <stdio.h> 
#include <stdlib.h>
#include <cstring>
#include <time.h> 

__global__ void mem_trs_test(int * input){
// 1D array with 2 1D thread blocks 
	int gid = blockIdx.x * blockDim.x + threadIdx.x; 
	printf("tid: %d, gid: %d, value: %d \n", threadIdx.x, gid, input[gid]); 


}

// kernel with restriction of not using all threads if array size < thread number
__global__ void mem_trs_test2(int * input, int size){
// 1D array with 2 1D thread blocks 
	int gid = blockIdx.x * blockDim.x + threadIdx.x; 
	if(gid < size){
		printf("tid: %d, gid: %d, value: %d \n", threadIdx.x, gid, input[gid]); 
	}
	


}


int main(){
	int size = 150;  // 128 for mem_trs_test
	int byte_size = size * sizeof(int); 
	// Allocate host memory
	int * h_input; 
	h_input = (int*)malloc(byte_size); // malloc returns void * , and we cast it to int*
	// Initiallize array randomly
	time_t t ; 
	srand((unsigned)time(&t)); 
	for(int i = 0; i < size; i++){
		h_input[i] = (int)(rand() & 0xff);  // random value between 0 and 255
	}
	// Allocate memory for the array in the device 
	int * d_input; 
	// cudaMalloc has double pointer as first argument , then specify the size of the memory
	cudaMalloc((void**)&d_input,byte_size); 
	// transfer the initialized array from the host to device 
	cudaMemcpy(d_input, h_input, byte_size, cudaMemcpyHostToDevice);
	// kernel launch parameters
	dim3 block(32); // 64 for mem_trs_test
	dim3 grid(5); // 2 for mem_trs_test

	//mem_trs_test << < grid,block >> > (d_input); 
	mem_trs_test2 << < grid,block >> > (d_input, size); 
	cudaDeviceSynchronize(); 
	// Reclaim memory 
	cudaFree(d_input); 
	free(h_input); 

	cudaDeviceReset();
	return 0; 
}

