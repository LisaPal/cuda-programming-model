#include "cuda_runtime.h"
#include "device_launch_parameters.h"

#include <stdio.h>
#include <stdlib.h>

// Video 10 + 11

// Video 10

__global__ void unique_gid_calculation_2d(int * input){

	int tid = threadIdx.x; 


	int block_offset = blockIdx.x * blockDim.x; 
	int row_offset = blockDim.x * gridDim.x * blockIdx.y ; 
	int gid = row_offset + block_offset + tid; // gid = global id 
	printf("blockIdx.x : %d, blockIdx.y : %d , threadIdx.x : %d , gid : %d, value: %d \n", 
	blockIdx.x,blockIdx.y, tid, gid, input[gid]);

}

// Video 11 
__global__ void unique_gid_calculation_2d_2d(int * data){
	int tid = threadIdx.y * blockDim.x + threadIdx.x ;
	int num_threads_in_a_block = blockDim.x * blockDim.y ;  
	int block_offset = num_threads_in_a_block * blockIdx.x; 
	int num_threads_in_a_row = num_threads_in_a_block * gridDim.x; 
	int row_offset = num_threads_in_a_row * blockIdx.y; 
	int gid = row_offset + block_offset + tid; // gid = global id 
	printf("blockIdx.x : %d, blockIdx.y : %d , threadIdx.x : %d , gid : %d, value: %d \n", 
	blockIdx.x,blockIdx.y, tid, gid, input[gid]); 

int main(){
	int array_size = 16; // when calling unique_idx_calc_threadIdx
	 int array_byte_size = sizeof(int) * array_size; 
	 int h_data[] = {23,9,4,53,65,12,1,33, 87,45,23,12,342,56,44,99};

	 for (int i = 0; i < array_size; i++){
		printf("%d ", h_data[i]);

	}

	printf("\n \n");
	int * d_data; 
	cudaMalloc((void**)&d_data, array_byte_size);
	cudaMemcpy(d_data,h_data,array_byte_size, cudaMemcpyHostToDevice); 

    // Video 10: 2D grid with 16 threads. 4 blocks with 4 threads each. 2 blocks in x-dim and 2 blocks in y-dim.  
	 // dim3 block(4); 
	 // dim3 grid(2,2); 
	//unique_gid_calculation_2d <<< grid,block >>> (d_data);
	// Video 11: 2D grid with 16 threads. 4 blocks with 2 threads in x dim and 2 in y dim. 
 	 dim3 block(2,2);
 	 dim3 grid(2,2); 
 	 unique_gid_calculation_2d_2d<<<grid, block>>>(d_data); 


 	 cudaDeviceSynchronize(); 
 	 cudaDeviceReset();
	return 0; 
}