#include "cuda_runtime.h"
#include "device_launch_parameters.h"

#include <stdio.h>
#include <stdlib.h> 

// Exercise 2: A randomly initialized 64 element array to pass to device. Launch 3D grid with 
//2 threads in each block dimension (2,2,2) and 2 blocks in each dimension, for a total of 8 
//threads in each dimension.


// Print out the array element values. Each thread should print a single value. 
__global__ void show_arr_elems_3d(int *input){
	int tid = (threadIdx.z * blockDim.x) + (threadIdx.y * blockDim.x * blockDim.y)+threadIdx.x; 
	int num_threads_in_a_row = (blockDim.x * blockDim.y * blockDim.z * gridDim.x); 
	int row_offset = num_threads_in_a_row + blockIdx.y; 
	int num_threads_in_a_block = blockDim.x * blockDim.y * blockDim.z; 
	int depth_offset = num_threads_in_a_block * blockIdx.z;
	int block_offset = num_threads_in_a_block * blockIdx.x; 

	int gid = row_offset + block_offset + depth_offset + tid; 
	printf("blockIdx.x : %d, blockIdx.y : %d, blockIdx.z: %d, threadIdx.x : %d , gid : %d, value: %d \n", 
	blockIdx.x,blockIdx.y, tid, gid, input[gid]);

}

int main(){
	int size = 64;
	int byte_size = size * sizeof(int); 
	// Allocate host memory
	int *h_input;s
	h_input = (int*)malloc(byte_size); 



	// Initialize array randomly
	time_t t;
	srand((unsigned)time(&t)); 
	for(int i = 0; i < size; i++){
		h_input[i] = (int)(rand() & 0xff);  // random value between 0 and 255
	} 

	// print data
	for (int i = 0; i < size; i++){
		printf("%d ", h_input[i]);

	}

	printf("\n \n");

	// Allocate memory for the array in the device
	int * d_input; 
	cudaMalloc((void**)&d_input,byte_size);
	// transfer initialized array from host to device 
	cudaMemcpy(d_input,h_input,byte_size,cudaMemcpyHostToDevice);

	// kernel launch parameters
	dim3 grid(2,2,2); 
	dim3 block(2,2,2); 
	// kernel launch 
	show_arr_elems_3d<<< grid, block >>>(d_input);
	cudaDeviceSynchronize(); 
	cudaDeviceReset();


	return 0; 
}



