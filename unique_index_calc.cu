#include "cuda_runtime.h"
#include "device_launch_parameters.h"

#include <stdio.h>
#include <stdlib.h>


// Video 9 
// Argument: pointer to the integer array . We are going to transfer memory from host to this pointer in the device.
__global__ void unique_idx_calc_threadIdx(int * input){

	int tid = threadIdx.x; // Since we only have one thread block.
	printf("threadIdx : %d, value : %d \n", tid, input[tid] );


}


// Kernel for Example 4: 1D grid with 16 threads, arranged into 4 thread blocks.
__global__ void unique_gid_calculation(int * input){

	int tid = threadIdx.x; 
	int offset = blockIdx.x * blockDim.x; 
	int gid = tid + offset; // gid = global id 
	printf("blockIdx.x : %d, threadIdx.x : %d , gid : %d, value: %d \n", 
		blockIdx.x, tid, gid, input[gid]); // gid is global id

}



int main(){
	/*
	//------- Example 1 :  1 block of 8 threads
	int array_size = 8; // when calling unique_idx_calc_threadIdx
	int array_byte_size = sizeof(int) * array_size; 
	//------ Memory transfer stuff ------
	int h_data[] = {23,9,4,53,65,12,1,33};  // unique_idx_calc_threadIdx
		for (int i = 0; i < array_size; i++){
		printf("%d ", h_data[i]);

	}

	printf("\n \n");


	int * d_data; 
	cudaMalloc((void**)&d_data, array_byte_size);
	cudaMemcpy(d_data,h_data,array_byte_size, cudaMemcpyHostToDevice); 
	// Example 1:
	 dim3 block(8); 
	 dim3 grid(1); 
	 // Example 2:

	 dim3 block(4);
	 dim3 grid(2); 
	 unique_idx_calc_threadIdx <<< grid,block >>> (d_data);
	 cudaDeviceSynchronize();

	 cudaDeviceReset(); 

	// ----------------------------------*/
	// ------ Example 3: 1D grid with 16 threads arranged in 4 thread blocks

	 int array_size = 16; // when calling unique_idx_calc_threadIdx
	 int array_byte_size = sizeof(int) * array_size; 
	 int h_data[] = {23,9,4,53,65,12,1,33, 87,45,23,12,342,56,44,99};

	 for (int i = 0; i < array_size; i++){
		printf("%d ", h_data[i]);

	}

	printf("\n \n");
	int * d_data; 
	cudaMalloc((void**)&d_data, array_byte_size);
	cudaMemcpy(d_data,h_data,array_byte_size, cudaMemcpyHostToDevice); 

    // Example 3:  
	 dim3 block(4); 
	 dim3 grid(4); 
 	 unique_gid_calculation_2d <<< grid,block >>> (d_data);
 	 cudaDeviceSynchronize(); 
 	 cudaDeviceReset();
	return 0; 
}

