#include "cuda_runtime.h"
#include "device_launch_parameters.h"

#include <stdio.h>

// Video 5
// device code (kernel)
__global__ void hello_cuda(){
	printf("Hello CUDA world \n");
}

// main is the host code. Launch kernel from host code. 
int main(){
	// Example 1: Launching the hello_cuda kernel with 32 threads, arranged into 8 thread blocks in X dimension, each having 4 threads. 
    // dim3 block(4); //x=4, by default y=1 and z=1 
    // dim3 grid(8); 
  
    // Example 2: Launching kernel with 2D grid, with 64 threads arranged into 16 threads in X dim and 4 threads in Y dim.
    int nx,ny; // number of threads in each dimension of our grid 
    nx = 16;
    ny = 4; 

    dim3 block(8,2); 
    dim3 grid(nx/block.x, ny/block.y); // Setting the number of blocks in each dimension dynamically at runtime




   // first param specifies the number of thread blocks in each dimension
   // second parameter specifies number of threads in each dimension of a block 
	hello_cuda << <grid,block >> > ();  //if secong parameter is 20, hello_cuda happens 20 times, 20 hellos printed
	cudaDeviceSynchronize(); 

	cudaDeviceReset(); 
	return 0; 
}

