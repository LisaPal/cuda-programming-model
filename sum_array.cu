#include "cuda_runtime.h"
#include "device_launch_parameters.h"

#include "cuda_common.cuh"
#include <stdio.h>
#include "common.h"
#include "math.h"

// For random initialization
#include <stdlib.h>
#include <time.h>

//For memset
#include <cstring>

__global__ void sum_array_gpu(int * a, int * b, int * c, int size){
	int gid = blockIdx.x * blockDim.x + threadIdx.x;
	if(gid < size){
		c[gid] = a[gid] + b[gid]; 
	}

}

void sum_array_cpu(int * a, int * b, int * c, int size){
	for(int i =0; i < size; i++){
		c[i] = a[i] + b[i]; 
	}

}

int main(){
	// 2 10,000 element array
	int size = 10000; 
	int block_size = 128;
	int num_bytes = size * sizeof(int); 
	

	// host pointers
	int * h_a, *h_b, *gpu_results,*h_c; 
	// allocate memory for each of these pointers
	h_a = (int*)malloc(num_bytes);  
	h_b = (int*)malloc(num_bytes);  
	gpu_results = (int*)malloc(num_bytes);
	h_c = (int*)malloc(num_bytes); 

	// randomly initialize a and b arrays 
	time_t t;
	srand((unsigned)time(&t));
	for(int i=0; i < size; i++){
		h_a[i] = (int)(rand() & 0xFF); // random values int range of 0 to 255
	}  

	for(int i=0; i < size; i++){
		h_b[i] = (int)(rand() & 0xFF);
	}  
	clock_t cpu_start, cpu_end;
	cpu_start = clock(); 
	sum_array_cpu(h_a,h_b,h_c,size);
	cpu_end = clock(); 

	memset(gpu_results,0,num_bytes); 


	// device pointer 
	int *d_a, *d_b, *d_c;
	// Another possible way to check error. But we will use our function that does this in cuda_common.cuh. 
	/*error = cudaMalloc((int**)&d_a,num_bytes);
	if(error != cudaSuccess){
		fprintf((stderr), " Error: %s\n", cudaGetErrorString(error) );
	}
	*/ 
	gpuErrchk(cudaMalloc((int**)&d_a,num_bytes));
	gpuErrchk(cudaMalloc((int**)&d_b,num_bytes));
	gpuErrchk(cudaMalloc((int**)&d_c,num_bytes)); 
	// kernel launch parameters
	dim3 block(block_size);
	dim3 grid((size/block.x)+1);// we added extra one because array size(10K)  will not be fully
	// divided by block size (128) . We add 1 to guarantee that we will have more threads than array size.
	clock_t htod_start, htod_end; 
	htod_start = clock(); 
	// We are transferring memory for the a and b device pointers from the host
	// We only tranfer a and b arrays to device since c will be populated by the operation in the kernel.
	cudaMemcpy(d_a,h_a,num_bytes,cudaMemcpyHostToDevice);
	cudaMemcpy(d_b,h_b,num_bytes,cudaMemcpyHostToDevice);
	htod_end = clock(); 
	// execution time measuring in GPU
	clock_t gpu_start, gpu_end;
	gpu_start = clock(); 
	// launching the grid
	sum_array_gpu << <grid,block >> > (d_a,d_b,d_c,size);
	cudaDeviceSynchronize(); 
	gpu_end = clock(); 
	clock_t dtoh_start, dtoh_end; 
	dtoh_start = clock();
	// transfer memory back to the host
	cudaMemcpy(gpu_results, d_c, num_bytes, cudaMemcpyDeviceToHost); 
	dtoh_end = clock(); 
	// array comparison
	// VALIDITY CHECK
	compare_arrays(gpu_results,h_c,size);

	printf("Sum array CPU execution time : %4.6f\n",
	(double)((double)cpu_end-cpu_start)/CLOCKS_PER_SEC);

	printf("Sum array GPU execution time : %4.6f\n",
	(double)((double)gpu_end-gpu_start)/CLOCKS_PER_SEC);

	printf("htod mem transfer time : %4.6f\n",
	(double)((double)htod_end-htod_start)/CLOCKS_PER_SEC);

	printf("dtoh mem transfer time : %4.6f\n",
	(double)((double)dtoh_end-dtoh_start)/CLOCKS_PER_SEC);

	printf("Sum array GPU total execution time : %4.6f\n",
	(double)((double)dtoh_end-dtoh_start)/CLOCKS_PER_SEC);

	// raclaim memory
	cudaFree(d_c);
	cudaFree(d_b);
	cudaFree(d_a);

	free(gpu_results);
	free(h_a);
	free(h_b);


	return 0; 
}


