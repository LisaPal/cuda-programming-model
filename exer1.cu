#include "cuda_runtime.h"
#include "device_launch_parameters.h"

#include <stdio.h> 
// Author: Lisa Pal, 26 January 2020. 
// Programming exercise 1: print value of threadIdx, blockIdx, gridDim variables for a 3D grid 
// which has 4 thread blocks in all X,Y,Z dimensions and thread block size will be 2 threads in 
// each dimension as well. 


// Kernel to print all the required values.
__global__ void print_details(){
	printf("threadIdx.x: %d, threadIdx.y: %d, threadIdx.z: %d, blockIdx.x: %d, blockIdx.y : %d, blockIdx.z: %d, gridDim.x: %d, gridDim.y: %d, gridDim.z: %d\n",
		threadIdx.x, threadIdx.y, threadIdx.z, blockIdx.x, blockIdx.y, blockIdx.z, gridDim.x, gridDim.y, gridDim.z);
}


int main(){
	// nx, ny and nz are the TOTAL number of threads in the entire grid, in each corresponding dimension.
	// 2 threads in each block, in each dimension. And 4 blocks in each dimension. 
	int nx, ny, nz; 
	nx = 8; ny = 8; nz = 8; 
	
	dim3 block(2,2,2); 
	// parameters: number of blocks in each dimension of the grid
	dim3 grid(nx/block.x, ny/block.y, nz/block.z);  

	// Launch kernel with launch parameters
    print_details << <grid,block >> > (); 

    // Wait until kernel execution is finished
    cudaDeviceSynchronize(); 

    cudaDeviceReset(); 

	return 0; 
}