# CUDA programming model
Small practice codes written while I was learning CUDA. Topics covered: 

- Basic elements of CUDA program
- Organization of threads in a CUDA program - threadIdx
- blockIdx, blockDum, gridDim
- Unique index calculation using threadIdx, blockIdx and blockDim
- Unique index calculation for 2D grid
- Memory transfer between host and device 
- Sum array example

